package classe;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Classe representant un professeur.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class Professeur extends Personne implements IDisponible{

	/**
	 * Attribut de la matiere enseignée par le professeur.
	 */
	private Matiere matiere;

	/**
	 * Attribut du calendrier du professeur.
	 */
	private Calendrier calendrier;

	/**
	 * constructeur de professeur avec son nom, son prenom et sa matiere enseignée.
	 * @param nom
	 * @param prenom
	 * @param matiere
	 */
	public Professeur(String nom, String prenom, Matiere matiere) {
		super(nom, prenom);
		this.matiere = matiere;
		this.calendrier = new Calendrier();
	}
	
	/**
	 * constructeur de professeur avec son nom, son prenom et sa matiere enseignée.
	 * @param nom
	 * @param prenom
	 * @param matiere
	 */
	public Professeur(String nom, String prenom, Matiere matiere,String mdp) {
		super(nom, prenom,mdp);
		this.matiere = matiere;
		this.calendrier = new Calendrier();
	}

	/**
	 * Constructeur vide de Professeur
	 */
	public Professeur() {
		super();
		this.matiere=null;
		this.calendrier=new Calendrier();
	}


	/**
	 * Constructeur de professeur avec son nom, son prenom, sa matiere et son calendrier.
	 * @param nom
	 * @param prenom
	 * @param matiere
	 * @param calendrier
	 */
	public Professeur(String nom, String prenom, Matiere matiere, Calendrier calendrier) {
		super(nom, prenom);
		this.matiere = matiere;
		this.calendrier = calendrier;
	}
	
	/**
	 * Constructeur complet de professeur avec son nom, son prenom, sa matiere son calendrier et son mot de passe.
	 * @param nom
	 * @param prenom
	 * @param matiere
	 * @param calendrier
	 * @param mdp
	 */
	public Professeur(String nom, String prenom, Matiere matiere, Calendrier calendrier,String mdp) {
		super(nom, prenom,mdp);
		this.matiere = matiere;
		this.calendrier = calendrier;
	}

	/**
	 * getter matiere
	 * @return la matiere enseignée par le professeur
	 */
	public Matiere getMatiere() {
		return this.matiere;
	}

	/**
	 * getter calendrier du professeur
	 * @return le calendrier 
	 */
	public Calendrier getCalendrier() {
		return this.calendrier;
	}

	/**
	 * setter matiere
	 * @param matiere
	 */
	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}

	/**
	 * setter calendrier
	 * @param calendrier
	 */
	public void setCalendrier(Calendrier calendrier) {
		this.calendrier = calendrier;
	}


	/**
	 * Methode d ajout d une note par un professeur dans les notes d un eleve.
	 * @param etudiant
	 * @param note
	 */
	public void noter(Note note) {
		Map<Matiere,List<Note>> aux = note.getEtudiant().getNotes();

		Set<Matiere> cles = note.getEtudiant().getNotes().keySet();
		Iterator<Matiere> it = cles.iterator();
		while(it.hasNext()) {
			Matiere cle = it.next();
			if (cle == this.matiere){
				List<Note> l = note.getEtudiant().getNotes().get(cle);
				l.add(note);
				aux.put(this.matiere,l);
				note.getEtudiant().setNotes(aux);
				return;
			}
		}
		List<Note> l =new ArrayList<Note>();
		l.add(note);
		aux.put(this.matiere, l);
		note.getEtudiant().setNotes(aux);
	}

	/**
	 * Methode qui permet de savoir les creneaux disponibles du professeur a partir d une date donnee
	 * du type Calendar.
	 * @return la liste des creneaux disponibles du professeur.
	 */
	@Override
	public List<Creneau> disponible(Calendar calendar) {
		List<Creneau> c =this.calendrier.getEmploiDuTemps();
		List<Creneau> l = new ArrayList<Creneau>();
		int j;
		for (j =0; j< c.size();j++) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(c.get(j).getDateDebut().getTime());
			if (c.get(j).getCours()==null) {
				l.add(c.get(j));
			}
		}
		return l;
	}

	/**
	 * Methode pour permettre une visualisation dans le terminal de l'emploi du temps du professeur.
	 * @return un String qui contient ce qu affiche le terminal: c'est a dire l emploi du temps du professeur.
	 */
	public String visualiserEmploiDuTempsProf() {
		Calendar today = Calendar.getInstance();	
		Calendrier cal = this.calendrier;
		int c = 0;
		while(c<cal.getEmploiDuTemps().size() && (cal.getEmploiDuTemps().get(c).getDateDebut()).compareTo(today)<0) {
			c++;
		}
		String e = "\tAujourd'hui\n\n";
		for (int i = c; i<cal.getEmploiDuTemps().size() && i<c+70 ;i++) {
			String s =""; 
			DateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");  
			String strDate2 = dateFormat2.format(cal.getEmploiDuTemps().get(i).getDateDebut().getTime());
			if (cal.getEmploiDuTemps().get(i).getDateDebut().get(Calendar.HOUR_OF_DAY)<=8) {
				s+="\n\t" + strDate2.substring(0,10) + "\n\n";
			}
			s+= strDate2.substring(11) + "\t";
			if (cal.getEmploiDuTemps().get(i).getCours()!=null) {
				s+=cal.getEmploiDuTemps().get(i).getCours().getSalle().getNom()+" , " +
						cal.getEmploiDuTemps().get(i).getCours().getPromo().getId();
			}
			else {
				s+="--";
			}
			e+=s+"\n";
		}
		return(e);
	}
}
