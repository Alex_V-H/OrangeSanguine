package classe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Classe representant un calendrier.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class Calendrier {
	
	/**
	 * Attribut qui correspond a l emploi du temps,
	 * c est a dire l ensemble des creneaux.
	 */
	private List<Creneau> emploiDuTemps;
	
	/**
	 * Constructeur vide de Calendrier.
	 */
	public Calendrier() {
//		Dates de debut et de fin qui correspondent a la premiere date 
//		du calendrier et a la derniere.
		String dDebut = "01-09-2018-08:00:00";
		String dFin = "06-07-2019-08:00:00";
		List<Calendar> calendars = new ArrayList<Calendar>();
		//formatteur de date afin que ce soit homogene et facilement utilisable
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");
		try {
			Date dateDebut =sdf.parse(dDebut);
			Date dateFin =sdf.parse(dFin);
			int i, c;
			for(i=0,c=0;c<2000;i++) {
				Calendar calFin = Calendar.getInstance();
				calFin.setTime(dateFin);
				Calendar cal = Calendar.getInstance();
				cal.setTime(dateDebut);
				cal.add(Calendar.HOUR, i*1);
				//on verifie que la date recuperer est une date qui n est pas le week end et dans
				//les horaires de cours soit 8-12 14-18
				if ( ((cal.get(Calendar.HOUR_OF_DAY)>=14 && cal.get(Calendar.HOUR_OF_DAY)<=18)
						|| (cal.get(Calendar.HOUR_OF_DAY)>=8 && cal.get(Calendar.HOUR_OF_DAY)<=12))
						&& (cal.get(Calendar.DAY_OF_WEEK)!=(Calendar.SATURDAY))
						&& (cal.get(Calendar.DAY_OF_WEEK)!=((Calendar.SUNDAY)))){
					calendars.add(cal);
					c++;
				}
				if (cal.compareTo(calFin)>=0) {
					break;
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
			}
		List<Creneau> lCreneau = new ArrayList<Creneau>();
		for (Calendar c : calendars) {
			lCreneau.add(new Creneau(c));	
		}
		this.emploiDuTemps = lCreneau;
	}
	
	/**
	 * Constrcteur de Calendrier avec un emploi du temps.
	 * @param emploiDuTemps
	 */
	public Calendrier(List<Creneau> emploiDuTemps) {
		this.emploiDuTemps = emploiDuTemps;
	}

	/**
	 * getteur pour obtenir l emploi du temps.
	 * @return
	 */
	public List<Creneau> getEmploiDuTemps() {
		return this.emploiDuTemps;
	}

	/**
	 * setteur pour changer l emploi du temps.
	 * @param emploiDuTemps
	 */
	public void setEmploiDuTemps(List<Creneau> emploiDuTemps) {
		this.emploiDuTemps = emploiDuTemps;
	}
}
