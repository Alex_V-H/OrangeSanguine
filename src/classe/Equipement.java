package classe;

/**
 * Enumeration representant un equipement.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public enum Equipement {
	VIDEOPROJECTEUR,
	LOGICIEL,
	ORDINATEUR,
	LAVABO,
	TABLEAUCRAIE,
	TABLEAUVELLEDA;
}
