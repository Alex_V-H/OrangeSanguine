package classe;


/**
 * Classe representant une note.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class Note {

	/**
	 * Attribut qui correspond a l etudiant qui a eu cette note.
	 */
	private Etudiant etudiant;

	/**
	 * Attribut qui correspond a la matiere de cette note.
	 */
	private Matiere matiere;

	/**
	 * Attribut qui correspond a la valeur de cette note.
	 */
	private double valeur;

	/**
	 * Attribut qui correspond au coefficient de cette note.
	 */
	private int coeff;

	/**
	 * Constructeur de note avec un etudiant, une matiere,
	 * une valeur de note et son coefficient.
	 * @param etudiant
	 * @param matiere
	 * @param note
	 * @param coeff
	 */
	public Note(Etudiant etudiant, Matiere matiere, double note, int coeff) {
		this.etudiant = etudiant;
		this.matiere = matiere;
		this.valeur = note;
		this.coeff = coeff;
	}

	/**
	 * getter de l etudiant
	 * @return l etudiant qui a obtenu cette note
	 */
	public Etudiant getEtudiant() {
		return this.etudiant;
	}

	/** 
	 * getter de la matiere
	 * @return la matiere
	 */
	public Matiere getMatiere() {
		return this.matiere;
	}

	/**
	 * getter de la valeur de la note
	 * @return la valeur
	 */
	public double getValeur() {
		return this.valeur;
	}

	/**
	 * getter du coeff de la note
	 * @return le coeff
	 */
	public int getCoeff() {
		return this.coeff;
	}

	/**
	 * setter de l etudiant
	 * @param etudiant
	 */
	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}

	/**
	 * setter de la matiere
	 * @param matiere
	 */
	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}

	/**
	 * setter de la valeur de la note
	 * @param valeur
	 */
	public void setValeur(double valeur) {
		this.valeur = valeur;
	}

	/**
	 * setter du coeff de la note
	 * @param coeff
	 */
	public void setCoeff(int coeff) {
		this.coeff = coeff;
	}
}