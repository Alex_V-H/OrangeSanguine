package classe;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * Classe representant une promotion.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class Promotion implements IDisponible{
	
	/**
	 * Attribut qui correspond a la liste des etudiants de la promotion.
	 */
	private List<Etudiant> etudiants;
	
	/**
	 * Attribut qui correspond au calendrier de la promotion.
	 */
	private Calendrier calendrier;
	
	/**
	 * Attribut qui correspond a l identifiant de la promotion.
	 */
	private String id;

	/**
	 * Constructeur vide de Promotion
	 */
	public Promotion() {
		this.etudiants = new ArrayList<Etudiant>();
		this.calendrier = new Calendrier();
		this.id="";
	}

	
	/**
	 * Constructeur de promotion avec l id de Promotion
	 */
	public Promotion(String id) {
		this.etudiants = new ArrayList<Etudiant>();
		this.calendrier = new Calendrier();
		this.id=id;
	}

	/**
	 * Constructeur de promotion avec une liste d etudiants et un identifiant de promotion.
	 * @param etudiants
	 * @param calendrier
	 */
	public Promotion(List<Etudiant> etudiants, String id) {
		this.etudiants = etudiants;
		this.id=id;
		this.calendrier = new Calendrier();
	} 

	/**
	 * Constructeur de promotion avec une liste d etudiants, un calendrier et un identifiant de promotion
	 * @param etudiants
	 * @param calendrier
	 */
	public Promotion(List<Etudiant> etudiants, Calendrier calendrier, String id) {
		this.id=id;
		this.etudiants = etudiants;
		this.calendrier = calendrier;
	} 

	/**
	 * getter de la liste d etudiant de la promo
	 * @return
	 */
	public List<Etudiant> getEtudiants() {
		return this.etudiants;
	}

	/**
	 * getter du calendrier de la promo
	 * @return
	 */
	public Calendrier getCalendrier() {
		return this.calendrier;
	}

	/**
	 *getter de l id de la promo 
	 * @return id
	 */
	public String getId() {
		return this.id;
	}
	
	/**
	 * setter de l id de la promo
	 * @param id
	 */
	public void setId(String id) {
		this.id=id;
	}

	/**
	 * modification d un eleve dans la liste d etudiant
	 * l aoute s il n y est pas, le modifie sinon
	 * @param etudiants
	 */
	public void setEtudiants(List<Etudiant> etudiant) {
		this.etudiants=etudiant;
	}

	/**
	 * setter du calendrier de la promotion
	 * @param calendrier
	 */
	public void setCalendrier(Calendrier calendrier) {
		this.calendrier = calendrier;
	}

	/**
	 * calcul le nombre d eleves dans la classe
	 * @return
	 */
	public int calculEffectif() {
		return this.etudiants.size();
	}

	/**
	 * Methode qui permet de savoir les creneaux disponibles de la promotion a partir d une date donnee
	 * du type Calendar.
	 * @return la liste des creneaux disponibles de la promotion.
	 */
	@Override
	public List<Creneau> disponible(Calendar calendar) {
		List<Creneau> c =this.calendrier.getEmploiDuTemps();
		List<Creneau> l = new ArrayList<Creneau>();
		int j;
		for (j =0; j< c.size();j++) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(c.get(j).getDateDebut().getTime());
			if (c.get(j).getCours()==null) {
				l.add(c.get(j));
			}
		}
	return l;
	}
	
	/**
	 * Redefinition de la methode toString qui permet un affichage de l identifiant de la promo
	 * lorsqu on l appelle.
	 */
	@Override
	public String toString() {
		return this.id;
	}
}
