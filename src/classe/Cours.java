package classe;


/**
 * Classe representant un cours.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class Cours {
	
	/**
	 * Attribut qui correspond a la salle ou a lieu le cours.
	 */
	private Salle salle;
	
	/**
	 * Attribut qui correspond au professeur qui donne le cours.
	 */
	private Professeur prof;
	
	/**
	 * Attribut qui correspond a la promotion qui a le cours.
	 */
	private Promotion promo;
	
	/**
	 * Attribut qui correspond a la matiere du cours.
	 */
	private Matiere matiere;	
	
	/** 
	 * Constructeur de cours avec une salle, un professeur, 
	 * une promotion et une matiere.
	 * @param salle
	 * @param prof
	 * @param promo
	 * @param matiere
	 */
	public Cours(Salle salle, Professeur prof, Promotion promo,
			Matiere matiere) {
		this.salle = salle;
		this.prof = prof;
		this.promo = promo;
		this.matiere = matiere;	
	}

	/**
	 * Constructeur de Cours
	 */
	public Cours() {
		super();
		this.salle=new Salle();
		this.prof=new Professeur();
		this.promo=new Promotion();
		this.matiere=null;
	}


	/**
	 * getter pour obtenir la salle
	 * @return salle
	 */
	public Salle getSalle() {
		return this.salle;
	}

	/**
	 * getter pour obtenir le professeur
	 * @return prof
	 */
	public Professeur getProf() {
		return this.prof;
	}

	/**
	 * getter pour obtenir la promotion
	 * @return promo
	 */
	public Promotion getPromo() {
		return this.promo;
	}

	/**
	 * getter pour obtenir la matiere
	 * @return matiere
	 */
	public Matiere getMatiere() {
		return this.matiere;
	}

	/**
	 * setter pour changer la salle
	 * @param salle
	 */
	public void setSalle(Salle salle) {
		this.salle = salle;
	}

	/**
	 * setter pour changer le professeur
	 * @param prof
	 */
	public void setProf(Professeur prof) {
		this.prof = prof;
	}

	/**
	 * setter pour changer la promotion
	 * @param promo
	 */
	public void setPromo(Promotion promo) {
		this.promo = promo;
	}

	/**
	 * setter pour changer la matiere
	 * @param matiere
	 */
	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}
	
	/**
	 * Methode qui permet de savoir s'il s'agit de deux cours égaux.
	 * @param c
	 * @return (boolean) s'il s'agit ou non du même cours.
	 */
	public boolean equals(Cours c ) {
		if (c.getSalle() == this.salle && c.getMatiere()==this.matiere && 
				c.getPromo()==this.promo && c.getProf()==this.prof) {
			return true;
		}
		return false;
	}

}
