package interfaceGraphique;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import classe.Etudiant;
import java.awt.Graphics;

/**
 * Classe qui correspond à l'onglet Bulletin dans la fenêtre etudiants de l'interface graphique
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
@SuppressWarnings("serial")
public class PanneauBulletin extends JPanel implements ActionListener{

	/**
	 * Attribut qui correspond au bouton de telechargement du bulletin
	 */
	private JButton boutonTelechargement;

	/**
	 * Attribut qui correspond à la zone de texte ou s'affiche le bulletin
	 */
	private JTextPane bulletin;

	/**
	 * Attribut qui correspond a la barre de defilement de la zone de texte
	 */
	private JScrollPane defilement;

	/**
	 * Attribut correspondant à l'utilisateur élève
	 */
	private Etudiant eleve;

	/**
	 * Constructeur de PanneauBulletin
	 * @param user
	 */
	public PanneauBulletin(Etudiant user) {
		super();
		this.propPanneauBulletin(user);
	}
	
	/**
	 * Méthode permettant d initialiser toutes les proprietes du panneauBulletin
	 * @param user
	 */
	private void propPanneauBulletin(Etudiant user){
		this.setLayout(null);
		this.setPreferredSize(new Dimension(1000,600));
		this.eleve=user;
		this.propBulletin();
		this.propDefilement();
		this.propBoutonTelechargement();
	}
	
	
	/**
	 * Methode qui permet de mettre une image en arriere plan dans le panneau.
	 */
	@Override
	protected void paintComponent(Graphics g) {
		String filePath  ="/bonfond.png";
		String courant = new File("").getAbsolutePath();
		String path = courant + filePath;
		try {
			BufferedImage image = ImageIO.read(new File(path));
			g.drawImage(image, 0, 0, null);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	} 

	/**
	 * Méthode permettant d initialiser toutes les proprietes du boutton Telechargement
	 */
	private void propBoutonTelechargement() {
		this.boutonTelechargement=new JButton("Téléchargement Bulletin");
		this.boutonTelechargement.setBounds(500,500,300,30);
		this.add(this.boutonTelechargement);
		this.boutonTelechargement.addActionListener(this);
	}

	/**
	 * Méthode permettant d initialiser toutes les proprietes de l affichage du bulletin
	 */
	private void propBulletin() {
		String s = this.eleve.visualiserBulletin();
		this.bulletin=new JTextPane();
		this.bulletin.setText(s);
		this.bulletin.setFont(new Font(s,Font.PLAIN,17));
		this.bulletin.setEditable(false);
		this.add(this.bulletin);
		
	}

	/**
	 * Méthode permettant d initialiser toutes les proprietes de la barre de défilement de l'affichage
	 */
	private void propDefilement() {
		this.defilement = new JScrollPane(this.bulletin);
		this.defilement.setBounds(100,100,600,300);
		this.add(this.defilement);
		//permet que le curseur de defilement commence en haut
		this.bulletin.grabFocus();
		this.bulletin.setCaretPosition(0);

	}

	/**
	 * Méthode permettant l'affichage d'une fenêtre informative lorsque le bulletin est téléchargé
	 * @param eleve
	 */
	private static void bulletinDispo(Etudiant eleve) {
		JOptionPane.showMessageDialog(null,"OrangeSanguine_Bulletin" + eleve.getNom()+".txt\nest disponible dans le workspace.",
				"Information",JOptionPane.INFORMATION_MESSAGE);
	}

	
	/**
	 * Méthode permettant de gerer les actions dans le panneau
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==this.boutonTelechargement) {
			this.eleve.editerBulletin();
			bulletinDispo(this.eleve);
		}
	}

}