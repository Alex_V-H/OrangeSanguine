package interfaceGraphique;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import classe.Etudiant;

/**
 * Classe qui correspond à l'onglet Emploi du temps dans la fenêtre etudiant de l'interface graphique
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
@SuppressWarnings("serial")
public class PanneauEmploiDuTempsEtudiant extends JPanel{

	/**
	 * Attribut correspondant à la zone d'affichage de l'emploi du temps
	 */
	private JScrollPane defilementEDT;

	/**
	 * Attribut correpsondant à l'affichage de l'emploi du temps de l'éleve utilisateur
	 */
	private JTextPane edt;

	/** 
	 * Attribut correspondant à l'élève utilisateur
	 */
	private Etudiant eleve;

	/**
	 * Constructeur de PanneauEmploiDuTemps
	 * @param eleve
	 */
	public PanneauEmploiDuTempsEtudiant(Etudiant eleve) {
		super();
		this.proprieteEmploiDuTemps(eleve);
	}

	/**
	 * Méthode permettant d initialiser toutes les proprietes du 
	 * panneauEmploiDuTemps
	 * @param eleve
	 */
	private void proprieteEmploiDuTemps(Etudiant eleve) {
		this.eleve = eleve;
		this.setLayout(null);
		this.setPreferredSize(new Dimension(1000,600));
		this.propEdt();
		this.propDefilementEDT();
	}

	/**
	 * Methode qui permet de mettre une image en arriere plan dans le panneau.
	 */
	@Override
	protected void paintComponent(Graphics g) {
		String filePath  ="/bonfond.png";
		String courant = new File("").getAbsolutePath();
		String path = courant + filePath;
		try {
			BufferedImage image = ImageIO.read(new File(path));
			g.drawImage(image, 0, 0, null);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	} 

	/**
	 * Méthode permettant d'initialiser les propriétés du défilement de la zone d'affichage
	 */
	private void propDefilementEDT() {
		this.defilementEDT = new JScrollPane(this.edt);
		this.defilementEDT.setBounds(100,100,500,265);
		this.add(this.defilementEDT);
		//permet que le curseur de defilement commence en haut
		this.edt.grabFocus();
		this.edt.setCaretPosition(0);
	}

	/**
	 * Méthode permettant d'initialiser toutes les proprietes du JTextPane edt
	 */
	private void propEdt() {
		String s =this.eleve.visualiserEmploiDuTemps();
		this.edt = new JTextPane();
		this.edt.setText(s);
		this.edt.setFont(new Font(s,Font.PLAIN,17));
		this.edt.setEditable(false);
		this.add(this.edt);
	}

}