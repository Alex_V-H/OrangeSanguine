package interfaceGraphique;

import java.util.List;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import classe.Professeur;
import classe.Promotion;

/**
 * Classe qui correspond au contenu de la fenetre professeur dans l'interface graphique.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
@SuppressWarnings("serial")
public class PanneauProf extends JPanel{

	/**
	 * Attribut qui correspond a l onglet du panneau professeur
	 */
	private JTabbedPane onglets;

	/**
	 * Attribut qui correspond a l onglet emploi du temps dans le panneau professeur
	 */
	private PanneauEmploiDuTempsProf edt;

	/**
	 * Attribut qui correspond a l'onglet noter dans le panneau professeur
	 */
	private PanneauNoter panneauNoter;

	/**
	 * Constructeur de PanneauProf
	 * @param user
	 * @param promos
	 */
	public PanneauProf(Professeur user,List<Promotion> promos) {
		super();
		this.proprietePanneauProf(user,promos);
	}

	/**
	 * Methode permettant d initialiser toutes les proprietes du PanneauProf
	 * 
	 */
	private void proprietePanneauProf(Professeur user,List<Promotion> promos) {
		this.edt=new PanneauEmploiDuTempsProf(user);
		this.panneauNoter=new PanneauNoter(user,promos);
		this.propOnglets();
	}

	/**
	 * Methode permettant d initialiser toutes les proprietes des onglets 
	 * 
	 */
	private void propOnglets() {
		this.onglets=new JTabbedPane(SwingConstants.TOP);
		this.onglets.addTab("Emploi du Temps", this.edt);
		this.onglets.addTab("Noter", this.panneauNoter);
		this.onglets.setOpaque(true);
		this.add(this.onglets);
		this.updateUI();
	}

}
