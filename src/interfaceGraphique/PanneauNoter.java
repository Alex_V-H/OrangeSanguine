package interfaceGraphique;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import classe.Etudiant;
import classe.Note;
import classe.Professeur;
import classe.Promotion;


/**
 * Classe qui correspond au contenu de la fenetre professeur dans l'onglet noter dans l'interface graphique.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
@SuppressWarnings("serial")
public class PanneauNoter extends JPanel implements ActionListener,TreeSelectionListener{
	
	/**
	 * Attribut qui correspond a l'etiquette Note
	 */
	private JLabel labelNote;
	
	/**
	 * Attribut qui correspond a l'etiquette Coefficient
	 */
	private JLabel labelCoeff;
	
	/**
	 * Attribut qui correspond a l'etiquette Message
	 */
	private JLabel message;
	
	/**
	 * Attribut qui correspond au bouton Valider
	 */
	private JButton boutonValider;
	
	/**
	 * Attribut qui correspond a la zone de texte pour saisir la note
	 */
	private JTextField saisieNote;
	
	/**
	 * Attribut qui correspond a la zone de texte pour saisir le coefficient
	 */
	private JTextField saisieCoeff;
	
	/**
	 * Attribut qui correspond a la Note sous forme de String
	 */
	private String note;
	
	/**
	 * Attribut qui correspond au Coefficient sous forme de String
	 */
	private String coeff;
	
	/**
	 * Attribut qui correspond a l'etudiant
	 */
	private Etudiant etud;
	
	/**
	 * Attribut qui correspond au professeur
	 */
	private Professeur prof;
	
	/**
	 * Attribut qui correspond a l'etiquette Note
	 */
	private List<Promotion> promos;
	
	/**
	 * Attribut dictionnaire des promotions qui sert à l'initialisation de l'arbre
	 */
	private Hashtable<String,Promotion> mapPromo;
	
	/**
	 * Attribut qui correspond a l'arbre avec toutes les promotions dans lesquelles il y a tous les etudiants.
	 */
	private JTree arbrePromo;

	/**
	 * Attribut qui correspond a un panneau déroulant qui contient les promotions dans lesquelles 
	 * se trouvent tous les etudiants
	 */
	private JScrollPane defilementArbre;
	
	/**
	 * Constructeur de PanneauNoter
	 * @param user
	 * @param promos
	 */
	public PanneauNoter(Professeur user,List<Promotion> promos) {
		super();
		this.proprietePanneauNoter(user,promos);
	}
	
	/**
	 * Methode permettant d initialiser toutes les proprietes du PanneauNoter.
	 * @param user
	 * @param promos
	 */
	private void proprietePanneauNoter(Professeur user,List<Promotion> promos) {
		this.etud=new Etudiant("","");
		this.mapPromo = new Hashtable<String,Promotion>();
		this.promos = promos;
		this.prof = user;
		this.setLayout(null);
		this.propMapPromo();
		this.propArbrePromo();
		this.propDefilementArbre();
		this.propLabelNote();
		this.propSaisieNote();
		this.propLabelCoeff();
		this.propSaisieCoeff();
		this.propMessage();
		this.propBoutonValider();
		this.note="";
		this.coeff="";
	}
	
	
	/**
	 * Methode permettant d initialiser toutes les proprietes du JScrollPane
	 * defilementArbre
	 */
	private void propDefilementArbre() {
		this.defilementArbre = new JScrollPane(this.arbrePromo);
		this.defilementArbre.setBounds(5,5,250,590);
		this.add(this.defilementArbre);

	}
	
	/**
	 * Methode permettant d initialiser toutes les proprietes et valeurs de l arbre composé de toutes les promotions.
	 */
	private void propArbrePromo() {
		DefaultMutableTreeNode top =new DefaultMutableTreeNode("Promotions");
		DefaultMutableTreeNode category = null;
        DefaultMutableTreeNode element = null;
        Set<String> cles =this.mapPromo.keySet();
		Iterator<String> it = cles.iterator();	
		while (it.hasNext()) {
			String cle = it.next();
			category = new DefaultMutableTreeNode(this.mapPromo.get(cle).getId());
			for (int j=0;j<this.mapPromo.get(cle).getEtudiants().size();j++) {
				element = new DefaultMutableTreeNode(this.mapPromo.get(cle).getEtudiants().get(j).toString());
				category.add(element);
			}
			top.add(category);
		}
		this.arbrePromo= new JTree(top);
		this.arbrePromo.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		this.arbrePromo.addTreeSelectionListener(this);
		this.arbrePromo.setRootVisible(true);
		this.add(this.arbrePromo);
	}
	
	/**
	 * Methode permettant de remplir un dictionnaire afin d initialiser l arbre par la suite.
	 */
	private void propMapPromo() {
		for (int i=0;i<this.promos.size();i++) {	
			this.mapPromo.put(this.promos.get(i).getId(),this.promos.get(i));
		}
	}
	
	/**
	 * Methode permettant d initialiser l attribut etudiant a vide
	 */
	private void propEtudiant(){
		this.etud=new Etudiant("","");
	}
	
	/**
	 * Methode permettant d initialiser toutes les proprietes du JLabel 
	 * labelNote
	 */
	private void propLabelNote() {
		this.labelNote=new JLabel();
		this.labelNote.setBounds(330,200,350,20);
		this.labelNote.setText("Note");
		this.add(this.labelNote);
	}
	
	/**
	 * Methode permettant d initialiser toutes les proprietes du JLabel 
	 * labelCoeff
	 */
	private void propLabelCoeff() {
		this.labelCoeff = new JLabel();
		this.labelCoeff.setBounds(330,250,350,20);
		this.labelCoeff.setText("Coefficient");
		this.add(this.labelCoeff);
	}
	
	/**
	 * Methode permettant d initialiser toutes les proprietes du JLabel 
	 * labelMessage
	 */
	private void propMessage() {
		this.message = new JLabel();
		this.message.setBounds(270,400,650,50);
		this.message.setText("Il faut appuyer sur ENTER après la saisie d'un champs "
				+ "pour qu'il soit comptabilisé");
		this.message.setFont(new Font("Serif",Font.BOLD,17));
		this.message.setOpaque(false);
		this.add(this.message);
	}
	
	/**
	 * Methode permettant d initialiser toutes les proprietes du JButton
	 * boutonValider
	 */
	private void propBoutonValider() {
		this.boutonValider = new JButton();
		this.boutonValider.setText("Valider");
		this.boutonValider.setBounds(520,300,100,20);
		this.add(this.boutonValider);
		this.boutonValider.addActionListener(this);
	}
	
	/**
	 * Methode permettant d initialiser toutes les proprietes du JTextField 
	 * saisieNote
	 */
	private void propSaisieNote() {
		this.saisieNote = new JTextField();
		this.saisieNote.setBounds(470,200,200,20);
		this.add(this.saisieNote);
		this.saisieNote.addActionListener(this);
	}
	
	/**
	 * Methode permettant d initialiser toutes les proprietes du JTextField 
	 * saisieCoeff
	 */
	private void propSaisieCoeff() {
		this.saisieCoeff = new JTextField();
		this.saisieCoeff.setBounds(470,250,200,20);
		this.add(this.saisieCoeff);
		this.saisieCoeff.addActionListener(this);
	}
	
	/**
	 * Methode qui permet de mettre une image en arriere plan dans le panneau.
	 */
	@Override
	protected void paintComponent(Graphics g) {
        String filePath  = "/bonfond.png";
        String courant = new File("").getAbsolutePath();
        String path = courant + filePath;
        try {
            BufferedImage image = ImageIO.read(new File(path));
            g.drawImage(image, 0, 0, null);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    } 
	
	/**
	 * Methode permettant de recuperer l etudiant noté par le professeur
	 * @param s
	 */
	private void recupEtud(String[] s) {
		for (int i=0;i<this.promos.size();i++) {
			for (int j=0;j<this.promos.get(i).getEtudiants().size();j++) {
				if ((this.promos.get(i).getEtudiants().get(j).getNom().equals(s[0])) && (this.promos.get(i).getEtudiants().get(j).getPrenom().equals(s[1]))) {
					this.etud=this.promos.get(i).getEtudiants().get(j);
				}
			}
		}
	}
	
	/**
	 * Methode permettant l affichage d une fenetre informative lors d une saisie mauvaise. 
	 */
	private static void afficheRate() {
		JOptionPane.showMessageDialog(null,"Saisie raté.",
				"Information",JOptionPane.WARNING_MESSAGE);
	}
	
	/**
	 * Methode permettant l affichage d une fenetre informative lors d une saisie réussie. 
	 */
	private static void afficheReussi() {
		JOptionPane.showMessageDialog(null,"Saisie réussie.",
				"Information",JOptionPane.INFORMATION_MESSAGE);
	}
	
	/**
	 * Methode permettant l affichage d une fenetre informative lors d une saisie de coefficient incorrecte. 
	 */
	private static void coeffIncorrect() {
		JOptionPane.showMessageDialog(null,"Le coefficient doit être positif.",
				"Information",JOptionPane.WARNING_MESSAGE);
	}
	
	/**
	 * Methode permettant l affichage d une fenetre informative lors d une saisie de note incorrecte. 
	 */
	private static void noteIncorrecte() {
		JOptionPane.showMessageDialog(null,"La note doit être comprise entre 0 et 20.",
				"Information",JOptionPane.WARNING_MESSAGE);
	}
	
	/**
	 * Methode qui permet de reinitialiser la fenetre lors d une saisie fausse.
	 */
	public void saisieFausse() {
		this.arbrePromo.clearSelection();
		this.propEtudiant();
		this.reinitSaisie();
		this.updateUI();
	}
	
	/**
	 * Methode permettant de reinitialiser les attributs lors d une saisie fausse.
	 */
	public void reinitSaisie() {
		this.note="";
		this.coeff="";
		this.saisieNote.setText("");
		this.saisieCoeff.setText("");
		this.etud = new Etudiant("","");
	}
	
	/**
	 * Methode permettant de gerer les actions dans le panneau noter
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()== this.boutonValider) {
			if ((this.note.equals("")) || (this.coeff.equals("")) || (this.etud.getNom().equals(""))) {
				afficheRate();
				this.saisieFausse();
			}
			else if (Integer.parseInt(this.note)<0 ||Integer.parseInt(this.note)>20 ) {
				noteIncorrecte();
				if (Integer.parseInt(this.coeff)<0){
					coeffIncorrect();
				}
				this.saisieFausse();
			}
			else if(Integer.parseInt(this.coeff)<0) {
				coeffIncorrect();
				this.saisieFausse();
			}
			else {
				Note note = new Note(this.etud,this.prof.getMatiere(),Integer.parseInt(this.note),Integer.parseInt(this.coeff));
				this.prof.noter(note);
				afficheReussi();
				this.reinitSaisie();
			}
		}
		if (e.getSource()==this.saisieNote) {
			this.note=this.saisieNote.getText();
		}
		if(e.getSource()==this.saisieCoeff) {
			this.coeff=this.saisieCoeff.getText();
		}
	}
	
	/**
	 * Methode permettant 
	 */
	@Override
	public void valueChanged(TreeSelectionEvent e) {
		TreePath t=e.getPath();
		String b = t.toString().substring(1,t.toString().length()-1);
		if (e.getSource()==this.arbrePromo) {
			String[] c = b.split(", ");
			if (c.length==3) {
				String[] d = c[2].split(" ");
				this.recupEtud(d);				
			}
		}	
	}
}
