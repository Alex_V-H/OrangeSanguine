package interfaceGraphique;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.swing.JWindow;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import classe.Calendrier;
import classe.Creneau;
import classe.Equipement;
import classe.Etudiant;
import classe.Matiere;
import classe.Note;
import classe.PersonnelAdministratif;
import classe.Professeur;
import classe.Promotion;
import classe.Salle;

/**
 * Classe désignant la fenêtre d'utilisation 
 * de l'application pour le demarrage.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
@SuppressWarnings("serial")
public class FenetreDemarrage extends JWindow{

	/**
	 * attribut qui correspond au contenu de la fenetre de demarrage
	 */
	private PanneauDemarrage pan;
	
	/**
	 * attribut qui correspond a la liste de tous les professeurs
	 */
	private List<Professeur> professeurs;
	
	/**
	 * attribut qui correspond a la liste de tous les etudiants
	 */
	private List<Etudiant> etudiants;
	
	/**
	 * attribut qui correspond a la liste de tous les employes de l administration
	 */
	private List<PersonnelAdministratif> admins;
	
	/**
	 * attribut qui correspond a la liste de toutes les promotions
	 */
	private List<Promotion> promos;
	
	/**
	 * attribut qui correspond a la liste de toutes les salles
	 */
	private List<Salle> salles;

	/**
	 * Constructeur de FenetreAdmin
	 * @param salles 
	 */
	public FenetreDemarrage() {
		super();
		this.proprieteFenetreDemarrage();
	}
	
	/**
	 * Methode permettant d initialiser toutes les proprietes de la fenetre
	 */
	private void proprieteFenetreDemarrage() {
		this.setSize(350,188);
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(false);
		this.professeurs = new ArrayList<Professeur>();
		this.etudiants= new ArrayList<Etudiant>();
		this.promos = new ArrayList<Promotion>();
		this.admins = new ArrayList<PersonnelAdministratif>();
		this.salles = new ArrayList<Salle>();
		this.initialisation();
		this.pan=new PanneauDemarrage();
		this.setContentPane(this.pan);
		this.setVisible(true);
	}
	
	/**
	 * Methode permettant d initialiser les attributs en tache de fond
	 */
	private void initialisation() {
		@SuppressWarnings("rawtypes")
		SwingWorker sw = new SwingWorker(){
			protected Object doInBackground() throws Exception {
				initialisationDepuisFichier();
				return null;
			}
			public void done(){            
				if(SwingUtilities.isEventDispatchThread())
					suite();
			}         
		};
		sw.execute();
	}
	
	/**
	 * Methode permettant d initialiser tous les attributs professeurs etudiants promotions et administrateur
	 * par lecture de fichier
	 */
	private void initialisationDepuisFichier(){
		this.initProfs();
		this.initEtudiantsPromos();
		this.initAdmins();
	}
	
	/**
	 * Methode permettant d initialiser les attributs non stockes dans un fichier et de passer a la fenetre suivante
	 */
	private void suite() {
		this.initSalles();
		this.initNote();
		this.initEdt();
		this.setVisible(false);
		this.dispose();
		FenetreIdentification f = new FenetreIdentification(this.professeurs,this.etudiants,this.admins,this.promos,this.salles);
		f.setVisible(true);
	}
	
	/**
	 * Methode permettant d initialiser les professeurs par la lecture d un fichier
	 */
	private void initProfs() {
		String filePath  ="/Professeurs.txt";
		String courant =new File("").getAbsolutePath();
		String path = courant + filePath;
		Path p= Paths.get(path);
		try (BufferedReader reader = Files.newBufferedReader(p, StandardCharsets.UTF_8)) {
			String lines;
			while ((lines = reader.readLine()) != null) {
				String[] line = lines.split(" ; ");
				for (Matiere m : Matiere.values()) {
					if (m.toString().equals(line[3])){
						Professeur prof = new Professeur(line[0], line[1],m,line[2]);
						this.professeurs.add(prof);
					}
				}
			}
			reader.close();
		}
		catch (IOException e2) {
			e2.printStackTrace();
		}
	}
	
	/**
	 * Methode permettant d initialiser les professeurs par la lecture d un fichier
	 */
	private void initPromos(HashMap<String,List<Etudiant>> dico) {
		for (String cle:dico.keySet()) {
			Promotion promo =new Promotion(dico.get(cle),cle);
			this.promos.add(promo);
			for (Etudiant etud:dico.get(cle)) {
				etud.setPromo(promo);
			}
		}
	}

	/**
	 * Methode permettant d initialiser les etudiants et les promos par la lecture d un fichier
	 */
	private void initEtudiantsPromos() {
		HashMap<String,List<Etudiant>> dico = new HashMap<String,List<Etudiant>>();
		String filePath  ="/Etudiants.txt";
		String courant =new File("").getAbsolutePath();
		String path = courant + filePath;
		Path p= Paths.get(path);
		try (BufferedReader reader = Files.newBufferedReader(p, StandardCharsets.UTF_8)) {
			String lines;
			while ((lines = reader.readLine()) != null) {
				String[] line = lines.split(" ; ");
				if (line.length>=2) {
					Etudiant etud = new Etudiant(line[0], line[1],line[2]);
					if (dico.containsKey(line[3])) {
						dico.get(line[3]).add(etud);
					}
					else {
						ArrayList<Etudiant> aux = new ArrayList<Etudiant>();
						aux.add(etud);
						dico.put(line[3],aux);
					}
					etudiants.add(etud);
				}
				else {
				}
			}
			this.initPromos(dico);
			reader.close();

		}
		catch (IOException e2) {
			e2.printStackTrace();
		}
	}

	/**
	 * Methode permettant d initialiser le personel administrateur par la lecture d un fichier
	 */
	private void initAdmins() {
		String filePath  ="/PersonnelAdmin.txt";
		String courant =new File("").getAbsolutePath();
		String path = courant + filePath;
		Path p= Paths.get(path);
		try (BufferedReader reader = Files.newBufferedReader(p, StandardCharsets.UTF_8)) {
			String lines;
			while ((lines = reader.readLine()) != null) {
				String[] line = lines.split(" ; ");
				PersonnelAdministratif pa = new PersonnelAdministratif(line[0], line[1],line[2]);
				this.admins.add(pa);
			}
			reader.close();
		}
		catch (IOException e2) {

			e2.printStackTrace();
		}
	}

	/**
	 * Methode permettant d initialiser les salles
	 */
	private void initSalles() {
		//toutes les salles presentes dans l attribut salles
		Map <Equipement,Integer> equipements1 = new HashMap<Equipement,Integer>();
		equipements1.put(Equipement.VIDEOPROJECTEUR,1);
		Map <Equipement,Integer> equipements2 = new HashMap<Equipement,Integer>();
		equipements2.put(Equipement.VIDEOPROJECTEUR,2);
		Map <Equipement,Integer> equipements3 = new HashMap<Equipement,Integer>();
		equipements3.put(Equipement.LAVABO, 2);
		Salle salle1 = new Salle(25, "L207");
		Salle salle2 = new Salle(55, "M403", equipements1);
		Salle salle3 = new Salle(65, "L006", equipements3);
		Salle salle4 = new Salle(65, "L005", equipements1);
		Salle salle5 = new Salle(20, "L004", equipements1);
		Salle salle6 = new Salle(65, "Picard", equipements2);
		Salle salle7 = new Salle(17, "C002");
		this.salles.add(salle1);
		this.salles.add(salle2);
		this.salles.add(salle3);
		this.salles.add(salle4);
		this.salles.add(salle5);
		this.salles.add(salle6);
		this.salles.add(salle7);
	}

	/**
	 * Methode permettant d initialiser les notes d etudiants de maniere aleatoire
	 */
	private void initNote() {
		int maxNote =20;
		int minNote=0;
		int maxCoeff=5;
		int minCoeff=1;
		int maxNbNote=5;
		int minNbNote=1;
		for (int j=0;j<professeurs.size();j++) {
			Random randNbNote= new Random();		
			int nbNote =randNbNote.nextInt(maxNbNote-minNbNote+1)+minNbNote;	
			for (int k=0;k<nbNote;k++) {		
				Random randNote= new Random();
				int noteAlea = randNote.nextInt(maxNote-minNote+1)+minNote;
				Random randCoeff= new Random();
				int coeffAlea = randCoeff.nextInt(maxCoeff-minCoeff+1)+minCoeff;
				for (int i=0;i<etudiants.size();i++) {
					Note note = new Note(etudiants.get(i),professeurs.get(j).getMatiere(),noteAlea,coeffAlea);
					professeurs.get(j).noter(note);
				}
			}
		}
	}

	/**
	 * Metode permettant de recuperer une liste de String correspondant aux dates des calendriers depuis 
	 * la date actuelle jusqu a la date de fin des calendriers
	 * @return s 
	 */
	private ArrayList<String> recupStringDate() {
		Calendar now = Calendar.getInstance();
		ArrayList<String> s=new ArrayList<String>();
		if ((professeurs!=null)&&(professeurs.size()>0)) {
			Calendrier calendrier = professeurs.get(0).getCalendrier();
			List<Creneau> cren =calendrier.getEmploiDuTemps();
			for (int i=0;i<cren.size();i++) {
				int j=0;
				DateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");  
				Calendar cal =cren.get(i).getDateDebut();
				if (cal.compareTo(now)>=0) {
					s.add(j,dateFormat2.format(cal.getTime()));
					j++;
				}
			}
		}
		return s;
	}

	/**
	 * Methode permettant d ajouter des cours et donc initialiser les emplois du temps de maniere aleatoire
	 */
	private void initEdt() {
		PersonnelAdministratif pa = new PersonnelAdministratif("Morand","Myriam");
		ArrayList<String> heure=recupStringDate();
		int profMin=0;
		int profMax=this.professeurs.size()-1;
		int promoMin=0;
		int promoMax=this.promos.size()-1;	
		int salleMin=0;
		int salleMax=this.salles.size()-1;
		//on ajoute un seul cours par heure pour simplifier l insertion de cours et ne pas avoir 
		//de conflit
		for (int cpt =0;cpt<heure.size();cpt++) {
			Random profRand=new Random();
			int profAlea = profRand.nextInt(profMax-profMin+1)+profMin;
			Random promoRand=new Random();
			int promoAlea = promoRand.nextInt(promoMax-promoMin+1)+promoMin;
			Random salleRand=new Random();
			int salleAlea = salleRand.nextInt(salleMax-salleMin+1)+salleMin;
			if (salles.get(salleAlea).getNbPlaces()>=promos.get(promoAlea).getEtudiants().size()) {
				pa.ajouterCoursGraphique(heure.get(cpt), salles.get(salleAlea), professeurs.get(profAlea),
						promos.get(promoAlea),professeurs.get(profAlea).getMatiere());
			}
		}
	}
}

