package interfaceGraphique;

import javax.swing.JFrame;
import classe.Etudiant;

/**
 * Classe désignant la fenêtre d'utilisation 
 * de l'application pour les etudiants
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
@SuppressWarnings("serial")
public class FenetreEtudiant extends JFrame{

	/**
	 * attribut qui correspond au contenu de la fenêtre des etudiants.
	 */
	private PanneauEtudiant pan;

	/**
	 * Constructeur de FenetreEleve
	 * @param user
	 */
	public FenetreEtudiant(Etudiant user) {
		super();
		this.proprieteFenetreEleve(user);
	}

	/**
	 * Méthode permettant d initialiser toutes les proprietes de la fenetre
	 * @param user
	 */
	private void proprieteFenetreEleve(Etudiant user) {
		this.setSize(1000,600);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setAlwaysOnTop(false);
		this.pan=new PanneauEtudiant(user);
		this.setContentPane(pan);
	}
}