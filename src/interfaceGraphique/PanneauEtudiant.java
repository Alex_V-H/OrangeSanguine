package interfaceGraphique;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import classe.Etudiant;

/**
 * Classe qui correspond au contenu de la fenêtre des etudiants dans l'interface graphique
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
@SuppressWarnings("serial")
public class PanneauEtudiant extends JPanel{

	/**
	 * Attribut coorespondant aux onglets de la fenetre
	 */
	private JTabbedPane onglets;

	/**
	 * Attribut correspondant à l'onglet emploi du temps de la fenetre élève
	 */
	private PanneauEmploiDuTempsEtudiant edt;

	/**
	 * Attribut correspondant à l'onglet de gestion des cours de la fenetre élève
	 */
	private PanneauBulletin bulletin;

	/**
	 * Constructeur de PanneauEleve
	 * @param user
	 */
	public PanneauEtudiant(Etudiant user) {
		super();
		this.proprietePanneauEleve(user);
	}

	/**
	 * Méthode permettant d initialiser toutes les proprietes du panneauEleve
	 * @param user
	 */
	private void proprietePanneauEleve(Etudiant user) {
		this.edt=new PanneauEmploiDuTempsEtudiant(user);
		this.bulletin = new PanneauBulletin(user);
		this.propOnglets();
	}

	/**
	 * Méthode permettant d initialiser toutes les proprietes des onglets
	 */
	private void propOnglets() {
		this.onglets=new JTabbedPane(SwingConstants.TOP);
		this.onglets.addTab("Emploi du Temps", this.edt);
		this.onglets.addTab("Bulletin", this.bulletin);
		this.onglets.setOpaque(true);
		this.add(this.onglets);
		

	}

}