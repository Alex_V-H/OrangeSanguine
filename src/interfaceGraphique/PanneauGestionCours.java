package interfaceGraphique;


import classe.Matiere;
import classe.PersonnelAdministratif;
import classe.Professeur;
import classe.Promotion;
import classe.Salle;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * Classe representant le panneau de gestion d'ajout et de suppression des cours 
 * dans la fenetre graphique.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 */
@SuppressWarnings("serial")
public class PanneauGestionCours extends JPanel implements ActionListener{

	/**
	 * Attribut qui correspond a une boite qui contient toutes les dates disponibles
	 * pour les items selectionnes
	 */
	private JComboBox<String> boxDate;

	/**
	 * Attribut qui correspond a une boite qui contient tous les professeurs
	 */
	private JComboBox<Professeur> boxProf;

	/**
	 * Attribut qui correspond a une boite qui contient toutes les salles
	 */
	private JComboBox<Salle> boxSalle;

	/**
	 * Attribut qui correspond a une boite qui contient toutes les promotions
	 */
	private JComboBox<Promotion> boxPromo;

	/**
	 * Attribut qui correspond a une boite qui contient toutes les matieres
	 */
	private JComboBox<Matiere> boxMatiere;

	/**
	 * Attribut qui correspond au bouton Ajouter
	 */
	private JButton boutonAjoute;

	/**
	 * Attribut qui correspond au bouton Supprimer
	 */
	private JButton boutonSupprime;

	/**
	 * Attribut qui correspond au bouton Valider
	 */
	private JButton boutonValide;

	/**
	 * Attribut qui correspond a l etiquette date
	 */
	private JLabel labelDate;

	/**
	 * Attribut qui correspond a l etiquette professeur
	 */
	private JLabel labelProf;

	/**
	 * Attribut qui correspond a l etiquette salle
	 */
	private JLabel labelSalle;

	/**
	 * Attribut qui correspond a l etiquette promotion
	 */
	private JLabel labelPromo;

	/**
	 * Attribut qui correspond a l etiquette matiere
	 */
	private JLabel labelMatiere;

	/**
	 * Attribut qui correspond a l indice dans la liste de dates proposées dans le menu deroulant
	 */
	private int rDate;

	/**
	 * Attribut qui correspond a la date selectionnee dans la fenetre
	 */
	private String dateSelec;

	/**
	 * Attribut qui correspond au professeur selectionne dans la fenetre
	 */
	private Professeur profSelec;

	/**
	 * Attribut qui correspond a la salle selectionnee dans la fenetre
	 */
	private Salle salleSelec;

	/**
	 * Attribut qui correspond a la promotion selectionnee dans la fenetre
	 */
	private Promotion promoSelec;

	/**
	 * Attribut qui correspond a la matiere selectionnee dans la fenetre
	 */
	private Matiere matiereSelec;

	/**
	 * Attribut qui correspond a la liste de tous les professeurs
	 */
	private List<Professeur> professeurs;

	/**
	 * Attribut qui correspond a la liste de toutes les promotions
	 */
	private List<Promotion> promotions;

	/**
	 * Attribut qui correspond a la liste de toutes les salles
	 */
	private List<Salle> salles;

	/**
	 * Attribut qui correspond a la liste de toutes les dates
	 */
	private List<String> dates ;

	/**
	 * Attribut qui correspond au personnel administratif qui est connecte 
	 * et qui est sur le panneau de gestion de cours
	 */
	private PersonnelAdministratif pa;

	/**
	 * Attribut du type Calendar qui correspond a la date d aujourd hui: c est celle a 
	 * partir de laqulle on va regarder l emploi du temps
	 */
	private Calendar calCours;

	/**
	 * Attribut du type String qui correspond a la date d aujourd hui: c est celle a 
	 * partir de laqulle on va regarder l emploi du temps
	 */
	private String dDebut;

	/**
	 * Attribut qui correspond au format de la date dans le type String
	 */
	private SimpleDateFormat sdf;

	/**
	 * Attribut qui correspond a la date d aujourd hui dans le type Date
	 */
	private Date dateDebut;

	/**
	 * Attribut qui permet de savoir si l utilisateur a deja apppuyé sur le bouton ajouter
	 */
	private boolean statutAjout;

	/**
	 * Attribut qui permet de savoir si l utilisateur a deja apppuyé sur le bouton supprimer
	 */
	private boolean statutSuppr;

	/**
	 * Constructeur de PanneauGestionCours
	 * @param promos 
	 * @param profs 
	 * @param salles2 
	 */
	public PanneauGestionCours(PersonnelAdministratif user, List<Professeur> profs, List<Promotion> promos, List<Salle> salles) {
		super();
		this.propPanneauGestionCours(user, profs, promos,salles);
	}

	/**
	 * Methode permettant d initialiser toutes les proprietes du
	 * PanneauGestionCours
	 * @param promos 
	 * @param profs 
	 * @param salles2 
	 */
	private void propPanneauGestionCours(PersonnelAdministratif user, List<Professeur> profs, List<Promotion> promos, List<Salle> salles) {
		this.dateSelec = new String();
		this.salleSelec = salles.get(0);
		this.promoSelec = promos.get(0);
		this.profSelec = profs.get(0);
		this.matiereSelec = Matiere.MATHEMATIQUES;
		this.dates= new ArrayList<String>();
		this.calCours = Calendar.getInstance();
		this.dDebut = "01-09-2019-08:00:00";
		this.sdf = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");
		this.dateDebut = null;
		try {
			this.dateDebut = this.sdf.parse(this.dDebut);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		this.calCours.setTime(this.dateDebut);
		this.pa = user;
		this.statutAjout=false;
		this.statutSuppr=false;
		this.professeurs=profs;
		this.promotions=promos;
		this.salles=salles;
		this.setLayout(null);
		this.propLabelDate();
		this.propBoxDate(new ArrayList<String>());
		this.propLabelProf();
		this.propBoxProf();
		this.propLabelSalle();
		this.propBoxSalle();
		this.propLabelPromo();
		this.propBoxPromo();
		this.propLabelMatiere();
		this.propBoxMatiere();
		this.propBoutonAjoute();
		this.propBoutonSupprime();
		this.propBoutonValide();
	}

	/**
	 * Methode qui permet de mettre une image en arriere plan dans le panneau.
	 */
	@Override
	protected void paintComponent(Graphics g) {
		String filePath  ="/bonfond.png";
		String courant = new File("").getAbsolutePath();
		String path = courant + filePath;
		try {
			BufferedImage image = ImageIO.read(new File(path));
			g.drawImage(image, 0, 0, null);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	} 

	/**
	 *Methode permettant d initialiser toutes les proprietes du comboBox boxDate
	 */
	private void propBoxDate(List<String> dates ) {
		String tab[]= new String[dates.size()];
		for (int i = 0;i < dates.size();i++) {
			tab[i] = dates.get(i);
		}
		this.boxDate=new JComboBox<String>(tab);
		this.boxDate.setBounds(360,360,200,20);
		this.boxDate.addActionListener(this);
		this.add(this.boxDate);
	}

	/**
	 *Methode permettant d initialiser toutes les proprietes du comboBox boxProf
	 */
	private void propBoxProf() {
		this.boxProf=new JComboBox<Professeur>();
		for (Professeur prof: this.professeurs) {
			this.boxProf.addItem(prof);
		}
		this.boxProf.setBounds(360,80,200,20);
		this.boxProf.addActionListener(this);
		this.add(this.boxProf);
	}

	/**
	 * Methode permettant d initialiser toutes les proprietes du comboBox
	 * boxSalle
	 */
	private void propBoxSalle(){
		this.boxSalle=new JComboBox<Salle>();
		for (Salle salle: this.salles) {
			this.boxSalle.addItem(salle);
		}
		this.boxSalle.setBounds(360,130,200,20);
		this.boxSalle.addActionListener(this);
		this.add(this.boxSalle);
	}

	/**
	 *Methode permettant d initialiser toutes les proprietes du comboBox
	 * boxPromo
	 */
	private void propBoxPromo() {
		this.boxPromo=new JComboBox<Promotion>();
		for (Promotion promo: this.promotions) {
			this.boxPromo.addItem(promo);
		}
		this.boxPromo.setBounds(360,180,200,20);
		this.boxPromo.addActionListener(this);
		this.add(this.boxPromo);
	}

	/**
	 *Methode permettant d initialiser toutes les proprietes du comboBox
	 * boxMatiere
	 */
	private void propBoxMatiere() {
		this.boxMatiere=new JComboBox<Matiere>(Matiere.values());
		this.boxMatiere.setBounds(360,230,200,20);
		this.boxMatiere.addActionListener(this);
		this.add(this.boxMatiere);
	}

	/**
	 * Methode permettant d initialiser toutes les proprietes du JButton
	 * bouttonAjoute
	 */
	private void propBoutonAjoute() {
		this.boutonAjoute=new JButton("Ajouter");
		this.boutonAjoute.setBounds(270,300,150,20);
		this.boutonAjoute.addActionListener(this);
		this.add(this.boutonAjoute);
	}

	/**
	 *Methode permettant d initialiser toutes les proprietes du JButton
	 * bouttonSupprime
	 */
	private void propBoutonSupprime() {
		this.boutonSupprime=new JButton("Supprimer");
		this.boutonSupprime.setBounds(500,300,150,20);
		this.boutonSupprime.addActionListener(this);
		this.add(this.boutonSupprime);
	}

	/**
	 * Methode permettant l affichage d une fenetre informative lorsque le
	 * prof selectionne n enseigne pas la matiere selectionnee. 
	 */
	private static void matiereRate() {
		JOptionPane.showMessageDialog(null,"Ce prof n'enseigne pas cette matière.",
				"Information",JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Methode permettant l affichage d une fenetre informative lorsque la
	 * salle selectionne n a pas assez de places pour la promotion
	 * selectionnee. 
	 */
	private static void salleRate() {
		JOptionPane.showMessageDialog(null,"Cette salle n'a pas assez de places.",
				"Information",JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Methode permettant l affichage d une fenetre informative lorsque le
	 * cours est bien ajoute. 
	 */
	private static void coursAjoute() {
		JOptionPane.showMessageDialog(null,"Cours ajouté.",
				"Information",JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Methode permettant l affichage d une fenetre informative lorsque le
	 * cours est bien supprime. 
	 */
	private static void coursSupprime() {
		JOptionPane.showMessageDialog(null,"Cours supprimé.",
				"Information",JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Methode permettant d initialiser toutes les proprietes du JButton
	 * bouttonAjoute
	 */
	private void propBoutonValide() {
		this.boutonValide=new JButton("Valider date");
		this.boutonValide.setBounds(360,450,200,20);
		this.boutonValide.addActionListener(this);
		this.add(this.boutonValide);
	}

	/**
	 *Methode permettant d initialiser toutes les proprietes du JLabel
	 * labelDate
	 */
	private void propLabelDate() {
		this.labelDate=new JLabel("Date");
		this.labelDate.setBounds(110,360,100,20);
		this.add(this.labelDate);
	}

	/**
	 *Methode permettant d initialiser toutes les proprietes du JLabel
	 * labelProf
	 */
	private void propLabelProf() {
		this.labelProf=new JLabel("Professeur");
		this.labelProf.setBounds(110,80,100,20);
		this.add(this.labelProf);
	}

	/**
	 * Methode permettant d initialiser toutes les proprietes du JLabel
	 * labelSalle
	 */
	private void propLabelSalle() {
		this.labelSalle=new JLabel("Salle");
		this.labelSalle.setBounds(110,130,100,20);
		this.add(this.labelSalle);
	}

	/**
	 * Methode permettant d initialiser toutes les proprietes du JLabel
	 * labelPromo
	 */
	private void propLabelPromo() {
		this.labelPromo=new JLabel("Promotion");
		this.labelPromo.setBounds(110,180,100,20);
		this.add(this.labelPromo);
	}

	/**
	 *Methode permettant d initialiser toutes les proprietes du JLabel
	 * labelMatiere
	 */
	private void propLabelMatiere() {
		this.labelMatiere=new JLabel("Matiere");
		this.labelMatiere.setBounds(110,230,100,20);
		this.add(this.labelMatiere);
	}

	/**
	 *Methode permettant de gerer les actions dans le panneauGestionCours
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		//prof selectionne
		if (e.getSource()==this.boxProf) {
			this.profSelec=(Professeur)this.boxProf.getSelectedItem();
		}
		//salle selecy=tionnee
		if (e.getSource()==this.boxSalle) {
			this.salleSelec=(Salle)this.boxSalle.getSelectedItem();
		}
		//promo selectionnee
		if(e.getSource()==this.boxPromo) {
			this.promoSelec=(Promotion)this.boxPromo.getSelectedItem();
		}
		//matiere selectionnee
		if(e.getSource()==this.boxMatiere) {
			this.matiereSelec = (Matiere) this.boxMatiere.getSelectedItem();
		}
		if (e.getSource()==this.boutonAjoute) {
			if (this.profSelec.getMatiere()!=(this.matiereSelec)) {
				matiereRate();
				if ((this.salleSelec.getNbPlaces()<(this.promoSelec.calculEffectif()))) {
					salleRate();
				}
			}
			else if (this.salleSelec.getNbPlaces()<(this.promoSelec.calculEffectif())) {
				salleRate();
			}
			else {
				this.dates = this.pa.datesDisponibles(this.calCours,this.salleSelec,this.profSelec,
						this.promoSelec,this.matiereSelec, this.salleSelec.getEquipements());
				this.dateSelec = this.dates.get(0);
				this.remove(boxDate);
				this.propBoxDate(this.dates);
				this.updateUI(); 
				this.statutAjout=true;
			}
		}
		if (e.getSource()==this.boxDate) {
			this.rDate=this.boxDate.getSelectedIndex();
			this.dateSelec = this.dates.get(this.rDate);
		}
		if (e.getSource()==this.boutonSupprime) {
			if ((this.profSelec.getMatiere()!=(this.matiereSelec))) {
				matiereRate();
				if (this.salleSelec.getNbPlaces()<(this.promoSelec.calculEffectif())) {
					salleRate();
				}
			}
			else if (this.salleSelec.getNbPlaces()<(this.promoSelec.calculEffectif())) {
				salleRate();
			}
			else {
				this.dates = this.pa.datesSupprimables(this.salleSelec,this.profSelec,this.promoSelec);
				this.dateSelec = this.dates.get(0);
				this.remove(this.boxDate);
				this.propBoxDate(this.dates);
				this.updateUI();
				this.statutSuppr=true;
			}
		}
		if (e.getSource()==this.boutonValide) {
			if (this.statutAjout) {
				this.pa.ajouterCoursGraphique(this.dateSelec, this.salleSelec,this.profSelec,this.promoSelec,this.matiereSelec);
				coursAjoute();
				this.dates = this.pa.datesDisponibles(this.calCours,this.salleSelec,this.profSelec,
						this.promoSelec,this.matiereSelec, this.salleSelec.getEquipements());
				this.statutAjout = false;
			}
			else if (this.statutSuppr) {
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");
				try {
					cal.setTime(sdf.parse(this.dateSelec));
				} catch (ParseException e1) {
					e1.printStackTrace();
				}
				this.pa.supprimerCours(cal, this.promoSelec);
				coursSupprime();
				this.dates = this.pa.datesSupprimables(this.salleSelec,this.profSelec,this.promoSelec);					
				this.statutSuppr = false;
			}
			this.remove(this.boxDate);
			this.propBoxDate(this.dates);
			this.updateUI();
		}
	}

}


