package interfaceGraphique;

import java.util.List;
import javax.swing.JFrame;
import classe.PersonnelAdministratif;
import classe.Professeur;
import classe.Promotion;
import classe.Salle;

/**
 * Classe désignant la fenêtre d'utilisation 
 * de l'application pour le personnel administratif.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
@SuppressWarnings("serial")
public class FenetreAdmin extends JFrame{
	/**
	 * attribut qui correspond au contenu de la fenetre du personnel administratif
	 */
	private PanneauAdmin pan;
	
	/**
	 * Constructeur de FenetreAdmin
	 * @param user
	 * @param profs
	 * @param promos
	 * @param salles
	 */
	public FenetreAdmin(PersonnelAdministratif user,List<Professeur> profs, List<Promotion> promos, List<Salle> salles) {
		super();
		this.proprieteFenetreAdmin(user, profs, promos,salles);
	}
	
	/**
	 * Méthode permettant d initialiser toutes les proprietes de la fenetre
	 * @param salles 
	 */
	private void proprieteFenetreAdmin(PersonnelAdministratif user, List<Professeur> profs, List<Promotion> promos, List<Salle> salles) {
		this.setSize(1000,600);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(false);
		this.pan=new PanneauAdmin(user, profs, promos,salles);
		this.setContentPane(pan);
	}
	
}