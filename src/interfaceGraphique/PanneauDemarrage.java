package interfaceGraphique;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import javax.swing.JPanel;

/**
 * Classe qui correspond au panneau de demarrage de l'interface graphique
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
@SuppressWarnings("serial")
public class PanneauDemarrage extends JPanel{

	/**
	 * Constructeur de PanneauDemarrage vide
	 */
	public PanneauDemarrage() {
		super();
	}
	
	/**
	 * Methode qui permet de mettre une image en arriere plan dans le panneau.
	 */
	@Override
	protected void paintComponent(Graphics g) {
        String filePath  ="/bonlogo.png";
        String courant = new File("").getAbsolutePath();
        String path = courant + filePath;
        try {
            BufferedImage image = ImageIO.read(new File(path));
            g.drawImage(image, 0, 0, null);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    } 
}
