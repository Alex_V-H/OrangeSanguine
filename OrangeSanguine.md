# Orange Sanguine

Orange Sanguine est une application de gestion d'emplois du temps et de notes pour un établissement scolaire.

## Pour commencer 
### Prérequis
Pour utilser Orange sanguine il faut utiliser un logiciel de lecture java de type [Eclipse](https://www.eclipse.org/downloads/) et une librairie,  [JRE JavaSE-1.7](https://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html).
### Installation
Pour installer Orange Sanguine,

1.  Depuis le dépot [GitlLab](https://gitlab.com/Alex_V-H/OrangeSanguine), copier le lien HTTPS comme suit:

![capture GitLab](https://docs.gitlab.com/ee/gitlab-basics/img/project_clone_url.png)

2. Ouvrir un **terminal** dans un repertoire X destiné à être le workspace. Saisir la commande suivante :
```
>>> git clone [lien https]
```
Le projet est maintenant importé dans le workspace.

3. Ouvrir le logiciel Eclipse en selectionnant le répertoire X comme workspace.

![captureEclipseworkspace](https://help.eclipse.org/2019-03/topic/org.eclipse.platform.doc.user/images/switch_workspace.png)

 
## Utilisation
### Utilisation Console
Dans le package *classe*, éxecuter le fichier *MainConsole*. Un scénario s'affiche dans la console.
Les explications sont entre étoiles :
```
** L'étudiant veut visualiser son emploi du temps **

Emploi du temps de l'étudiant VanHecke Alex (promoIT1)

MATIERE FRANCAIS
Note 7 Coeff 2

[...]
```
A l'ajout d'un cours par le personnel administratif, la console propose de saisir une disponibilité : 
``` 
Les disponibilités pour la semaine à venir sont :

		Aujourd'hui
15:00:00

[...]

Quel creneau choisissez-vous ? (dd-MM-yyyy-HH:mm:ss)

```
Saisir dans la console le créneau désiré parmis ceux proposés sous la forme dd-MM-yyyy-HH:mm:ss.

Exemple: 
```
Quel creneau choisissez-vous ? (dd-MM-yyyy-HH:mm:ss)

16-05-2019-14:00:00
```
Si le créneau saisi est incorrect un message d'erreur apparait, il faut recommencer la saisie.
```
Le creneau que vous avez saisi n'existe pas.
Veuillez recommencer:

Quel creneau choisissez-vous ? (dd-MM-yyyy-HH:mm:ss)
```
A la fin, un affichage des emplois du temps apparait sous cette forme :
```
Emploi du temps de l'étudiant.e : Enola Sengeissen (promo : IT1)

[...]

	16-05-2019
	
08:00:00  --
09:00:00  --
10:00:00  --
11:00:00  --
12:00:00  --
14:00:00  M403 , Benoit Costes
15:00:00  --
16:00:00  --
17:00:00  --
18:00:00  --
```

### Utilisation interface graphique
Dans le package _classe_, éxecuter le fichier _MainInterface_.

**Identification**

![PanneauIdentification](https://gitlab.com/Alex_V-H/OrangeSanguine/raw/master/Identification.tiff)

S'identifier en sélectionnant son statut dans le menu déroulant.
Entrer nom sous la forme *Nom* et son mot de passe en pressant Enter après chaque saisie. Presser `Valider`.

**Personnel Administratif**
Onglet *Emploi du Temps* : Sélectionner dans l'arbre la promotion, le professeur ou la salle dont vous voulez consulter l'emploi du temps. Vous visualisez l'emploi du temps sur une semaine.

Onglet  *Gestion de cours* : 

![PanneauGestionCours](https://gitlab.com/Alex_V-H/OrangeSanguine/raw/master/GestionCours.tiff)
Pour ajouter ou supprimer un cours : Choisissez les items du cours que vous voulez ajouter. Pressez `Ajouter` ou `Supprimer`. Choisissez ensuite parmis les dates proposées celle à laquelle vous voulez ajouter ou supprimer le cours. Presser `Valider date`. Les emplois du temps respectifs du professeur, de la promotion et de la salle choisis sont mis à jour.
Si la saisie est incorrecte ou que le cours n'est pas valide, un message d'erreur s'affiche, saisissez des items valides et appuyez de nouveau sur `Ajouter` ou `Supprimer`.

**Professeur**
Onglet *Noter* : 

![PanneauNoter](https://gitlab.com/Alex_V-H/OrangeSanguine/raw/master/PanneauNoter.tiff)
Sélectionnez dans l'arbre l'étudiant concerné. Saisissez une note ainsi qu'un coefficient en pressant Enter après chaque saisie. Pressez `Valider`.

Onglet *Emploi du temps* : Visualisez votre emploi du temps sur une semaine.

**Etudiant**
Onglet *Bulletin* : Vous avez accès à votre Bulletin. Un bouton `Téléchargement Bulletin` permet de le télécharger sous format texte dans le répertoire courant.

Exemple :
```
Bulletin de : VanHecke Alex

Edité le : 09-05-2019

Matière 		Moyenne

FRANCAIS 		10.7
MATHEMATIQUES 	15.5
BIOLOGIE 		9.0

Moyenne générale : 11.7
```

Onglet *Emploi du temps* : Visualisez votre emploi du temps sur une semaine.
## Auteurs
Sobreira Oriane
Van-Hecke Alex
Sengeissen Enola

Mai 2019
> Written with [StackEdit](https://stackedit.io/).
